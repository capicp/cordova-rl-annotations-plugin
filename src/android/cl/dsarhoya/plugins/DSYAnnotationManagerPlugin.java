/**
 */
package cl.dsarhoya.plugins;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.apache.cordova.PluginResult.Status;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import cl.dsarhoya.plugins.ContentViewerActivity;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import java.util.Date;

public class DSYAnnotationManagerPlugin extends CordovaPlugin {
  private static final String TAG = "DSYAnnotationManager";

  private static final String RECORD_AUDIO_PERMISSION = Manifest.permission.RECORD_AUDIO;
  private static final int ANNOTATIONS_PERMISSIONS_REQCODE = 9898;

  private static CallbackContext callbackContext;
  Activity activity;
  Context context;
  Resources resources;
  String packageName;

  String resourceUrl;

  public void initialize(CordovaInterface cordova, CordovaWebView webView) {
    super.initialize(cordova, webView);

    activity = cordova.getActivity();
    context = activity.getApplicationContext();
    resources = activity.getResources();
    packageName = context.getPackageName();

    Log.d(TAG, "Initializing DSYAnnotationManagerPlugin");
  }

  public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {

    if(action.equals("openResource")) {
      this.callbackContext = callbackContext;
      resourceUrl = args.getString(0);
      openResource(resourceUrl);

      cordova.setActivityResultCallback(this);

      return true;
    }

    if (action.equals("annotationSentOK")){
      activity.sendBroadcast(new Intent(ContentViewerActivity.ANNOTATION_SENT_ACTION));
      return true;
    }

    if (action.equals("annotationSentFail")){
      activity.sendBroadcast(new Intent(ContentViewerActivity.ANNOTATION_DELIVERY_FAILED_ACTION));
      return true;
    }

    return false;
  }

  private void openResource(final String url) throws JSONException{
    Log.d(TAG, "Se debe abrir " + url);

    if (cordova.hasPermission(RECORD_AUDIO_PERMISSION)){
      Intent intent = new Intent(context, ContentViewerActivity.class);
      intent.putExtra(ContentViewerActivity.PARAM_RESOURCE_URL, url);
      activity.startActivity(intent);
    }else{
      cordova.requestPermission(this, ANNOTATIONS_PERMISSIONS_REQCODE, RECORD_AUDIO_PERMISSION);
    }






    /*
    Runnable initUiRunnable = new Runnable() {
      @Override
      public void run() {

        activity.setContentView(resources.getIdentifier("activity_content_viewer", "layout", packageName));

        WebView contentView = activity.findViewById(resources.getIdentifier("webview", "id", packageName));
        contentView.setWebViewClient(new WebViewClient());
        contentView.loadUrl(url);

        Button closeButton = activity.findViewById(resources.getIdentifier("button_close", "id", packageName));
        closeButton.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            activity.finish();
          }
        });
      }
    };

    activity.runOnUiThread(initUiRunnable);
    */

  }

  public void onRequestPermissionResult(int requestCode, String[] permissions,
                                        int[] grantResults) throws JSONException
  {
    for(int r:grantResults)
    {
      if(r == PackageManager.PERMISSION_DENIED)
      {
        this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, false));
        return;
      }
    }
    openResource(resourceUrl);
  }

  public static void sendAnnotation(String type, String message){
    JSONObject annotation = new JSONObject();
    try {
      annotation.put("type", type);
      annotation.put("message", message);

      PluginResult result = new PluginResult(PluginResult.Status.OK, annotation);
      result.setKeepCallback(true);

      callbackContext.sendPluginResult(result);
    } catch (JSONException e) {
      e.printStackTrace();
    }


  }

}
