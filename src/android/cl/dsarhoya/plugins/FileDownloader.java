package cl.dsarhoya.plugins;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by capi on 12/11/17.
 */

public abstract class FileDownloader extends AsyncTask<Void, Void, Void> {

    private Context context;
    private File directory;
    private String fileURL;
    private String fileName;
    private File targetFile;

    public FileDownloader(Context ctx, String url, String fileName){
        this.context = ctx;
        this.fileURL = url;
        this.fileName = fileName;
        this.directory = context.getCacheDir();
        this.targetFile = new File(directory, fileName);
        targetFile.getParentFile().mkdirs();
    }

    @Override
    protected Void doInBackground(Void... params) {
        downloadFile();
        return null;
    }

    public void downloadFile(){
        try{
            int count;
            URL url = new URL(fileURL);
            URLConnection connection = url.openConnection();
            connection.connect();

            int file_size = connection.getContentLength();

            InputStream input = new BufferedInputStream(url.openStream(),8192);
            OutputStream output = new FileOutputStream(targetFile.getAbsoluteFile());

            byte data[] = new byte[1024];
            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (IOException e){
            Log.e(getClass().getSimpleName(), e.getMessage(), e);
            targetFile = null;
        }
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if (targetFile != null){
            onFileDownloaded(targetFile);
        }
        else{
            onDownloadFailed();
        }
    }

    protected abstract void onFileDownloaded(File file);
    protected abstract void onDownloadFailed();

    public File getDownloadedFile(){
        return targetFile;
    }
}

