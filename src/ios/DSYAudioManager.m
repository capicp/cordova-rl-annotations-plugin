//
//  DSYAudioManager.m
//  MyApp
//
//  Created by Alfredo Luco on 02-11-17.
//

#import "DSYAudioManager.h"
#import <AVFoundation/AVFoundation.h>

@implementation DSYAudioManager

//init with singleton pattern
+(instancetype)sharedInstace{
    
    static id shared = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

-(void)recorderConfigurationAtFileName:(NSString *)file{
    
    // Setting the audio file with our path (Documents of the iphone)
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               file,
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    // Setup audio session
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    // Define the recorder setting
    
    NSDictionary *recordSettings = @{AVEncoderAudioQualityKey: @(AVAudioQualityHigh),
                                     AVFormatIDKey: @(kAudioFormatMPEG4AAC),
                                     AVEncoderBitRateKey: @(320000),
                                     AVNumberOfChannelsKey: @(2),
                                     AVSampleRateKey: @(48000)};
    
    [self putResource:@"beep" withExtension:@"mp3"];
    [self putResource:@"beep2" withExtension:@"wav"];
    
    // Initiate and prepare the recorder
    
    _recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSettings error:NULL];
    _recorder.delegate = self;
    _recorder.meteringEnabled = YES;
    [_recorder prepareToRecord];
    
}

-(void)recordAudio{
    //init the session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setActive:YES error:nil];
    [_recorder record];
    
}

-(void)stopRecording{
    [_recorder stop];
}

-(NSArray *)getAudiosWithExtension:(NSString *)extension{
    NSArray* audios = @[];
    
    //get the directory path
    
    NSString *docPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    //init the file manager as default
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    //get the file list and get the .aac extension files
    
    NSArray *filelist= [filemgr contentsOfDirectoryAtPath:docPath error:nil];
    
    NSArray *filesAudios = [filelist pathsMatchingExtensions:@[extension]];
    
    for(NSString *file in filesAudios){
        
        //getting the path file from every file
        
        NSURL* dirUrl = [NSURL fileURLWithPath:docPath];
        NSURL* fileUrl = [NSURL URLWithString:file relativeToURL:dirUrl];
        
        //init the AVURLAsset
        AVURLAsset* audioAux = [AVURLAsset URLAssetWithURL:fileUrl options:nil];
        
        //append to audios's array
        
        audios = [audios arrayByAddingObject:audioAux];
    }
    
    NSLog(@"quantity of audios: %lu",(unsigned long)audios.count);
    
    return audios;
}

-(void)removeFiles{
    //Delete temp files
    
    //get the docPath
    NSString *docPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    //get the file list and get the .aac extension files
    
    NSArray *filelist= [filemgr contentsOfDirectoryAtPath:docPath error:nil];
    
    NSArray *filesAudios = [filelist pathsMatchingExtensions:@[@"m4a"]];
    
    //remove files
    
    NSError *error;
    
    for(NSString *file in filesAudios){
        
        //get the path from every file
        
        NSURL* dirUrl = [NSURL fileURLWithPath:docPath];
        NSURL* fileUrl = [NSURL URLWithString:file relativeToURL:dirUrl];
        
        //if the file is deletable, delete!
        
        if ([[NSFileManager defaultManager] isDeletableFileAtPath:[fileUrl path]]) {
            
            //success if the file was correctly removed
            
            BOOL success = [[NSFileManager defaultManager] removeItemAtPath:[fileUrl path] error:&error];
            
            //if not print an error
            
            if (!success) {
                
                NSLog(@"Error removing file at path: %@", error.localizedDescription);
                
            }
        }
        
    }
    
}

-(void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag{
    NSLog(@"Finish Recorded");
    [_recorder stop];
    [self.delegate didFinished];
}

-(void)playAudio:(NSURL *)url withAudioTracker:(BOOL)tracker{
    if([_recorder isRecording]){
        [self.recorder stop];
    }
    //setting the audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [session setActive:YES error:nil];
    [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];
    
    //setting the audioplayer and play it!
    _player = [[AVQueuePlayer alloc] init];
    [_player removeAllItems];
    
    AVPlayerItem* item = [[AVPlayerItem alloc] initWithURL:url];
    if(item)
        [_player insertItem:item afterItem:nil];
    
    if([url.pathExtension isEqual:@"wav"] && tracker){
        [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(audioPlayerDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];
    }
    _player.volume = [url.pathExtension  isEqual: @"wav"] ? 0.03 : 1.0f;
    [_player play];
}

-(void)audioPlayerDidFinishPlaying:(NSNotification*)notification{
    if(_recorder.isRecording){
        [self stopRecording];
    }else{
        [self recordAudio];
    }
    [NSNotificationCenter.defaultCenter removeObserver:notification];
}

-(void)putResource:(NSString*)name withExtension:(NSString*)extension{
    NSError* error;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:extension];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *mp3Path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@",name,extension]];
    
    [fileManager copyItemAtPath:path toPath:mp3Path error:&error];
}
@end
