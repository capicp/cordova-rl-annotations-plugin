//
//  DSYTextAnnotationView.h
//  MyApp
//
//  Created by Alfredo Luco on 31-10-17.
//

#import <UIKit/UIKit.h>

@protocol DSYTextAnnotationViewDelegate<NSObject>

-(void)didDeleteMessage;

@end

@interface DSYTextAnnotationView : UIView

@property (weak,nonatomic) id<DSYTextAnnotationViewDelegate> anotationDelegate;
@property (readwrite,nonatomic) UITextView* message;

-(instancetype)initWithMessage:(NSString*)message;

@end
