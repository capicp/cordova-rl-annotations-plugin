//
//  DSYAudioAnnotationView.m
//  MyApp
//
//  Created by Alfredo Luco on 02-11-17.
//

#import "DSYAudioAnnotationView.h"

@interface DSYAudioAnnotationView ()

@property (readwrite,nonatomic) UIButton* playButton;
@property (readwrite,nonatomic) UIButton* stopButton;
@property (readwrite,nonatomic) UIButton* deleteButton;

@end

@implementation DSYAudioAnnotationView

-(instancetype)init{
    if(!(self = [super init]))
        return nil;
    if(self){
        [self setup];
    }
    return self;
}

#pragma mark - setup
-(void)setup{
    [self setBackgroundColor:[UIColor lightGrayColor]];
    [self setupButtons];
    
    UIView* view = [[UIView alloc] init];
    [view addSubview:self.playButton];
    [view addSubview:self.stopButton];
    [view addSubview:self.deleteButton];
    
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[play(30)]-(16)-[stop(30)]-(16)-[delete(30)]-|" options:0 metrics:nil views:@{@"play" : self.playButton, @"stop" : self.stopButton, @"delete" : self.deleteButton}]];
    
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[play(30)]-|" options:0 metrics:nil views:@{@"play" : self.playButton}]];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[stop(30)]-|" options:0 metrics:nil views:@{@"stop" : self.stopButton}]];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[delete(30)]-|" options:0 metrics:nil views:@{@"delete" : self.deleteButton}]];
    
    view.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:view];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:1.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:1.0]];
}

-(void)setupButtons{
    self.playButton = [[UIButton alloc] init];
    [self.playButton setImage:[UIImage imageNamed:@"play-button.png"] forState:UIControlStateNormal];
    self.playButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.playButton addTarget:self action:@selector(play) forControlEvents:UIControlEventTouchUpInside];
    
    self.stopButton = [[UIButton alloc] init];
    [self.stopButton setImage:[UIImage imageNamed:@"stop-button.png"] forState:UIControlStateNormal];
    self.stopButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.stopButton addTarget:self action:@selector(stop) forControlEvents:UIControlEventTouchUpInside];
    
    self.deleteButton = [[UIButton alloc] init];
    [self.deleteButton setImage:[UIImage imageNamed:@"remove-symbol.png"] forState:UIControlStateNormal];
    self.deleteButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.deleteButton addTarget:self action:@selector(delete) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - selectors

-(void)play{
    if(self.delegate){
        [self.delegate didPlayAudio];
    }
}

-(void)stop{
    if(self.delegate){
        [self.delegate didStopAudio];
    }
}

-(void)delete{
    [self.delegate didDeleteAudio];
}

@end
