//
//  DSYAudioManager.h
//  MyApp
//
//  Created by Alfredo Luco on 02-11-17.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@protocol AudioManagerDelegate <NSObject>
@optional
-(void)didFinished;

@end

@interface DSYAudioManager : NSObject<AVAudioRecorderDelegate>
@property AVQueuePlayer* player;
@property AVAudioRecorder* recorder;
@property id<AudioManagerDelegate> delegate;
+(instancetype) sharedInstace;
-(void)playAudio:(NSURL *)url withAudioTracker:(BOOL)tracker;
-(void)recorderConfigurationAtFileName:(NSString*)file;
-(void)recordAudio;
-(void)stopRecording;
-(NSArray*)getAudiosWithExtension: (NSString*)extension;
-(void)removeFiles;
@end
