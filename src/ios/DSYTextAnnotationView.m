//
//  DSYTextAnnotationView.m
//  MyApp
//
//  Created by Alfredo Luco on 31-10-17.
//

#import "DSYTextAnnotationView.h"

@interface DSYTextAnnotationView ()

@property (readwrite,nonatomic) UIButton* closeButton;

@end

@implementation DSYTextAnnotationView

-(instancetype)initWithMessage:(NSString *)message{
    if(!(self = [super init])){
        return nil;
    }
    if(self){
        [self setup:message];
        [self buttonSetup];
    }
    return self;
}

#pragma mark - setup

-(void)setup:(NSString*)message{
    self.backgroundColor = [UIColor colorWithRed:0.459 green:0.455 blue:0.478 alpha:1.0];
    self.message = [[UITextView alloc] init];
    self.message.text = message;
    
    self.closeButton = [[UIButton alloc] init];
    
    [self addSubview:_message];
    [self addSubview:_closeButton];
    [self sendSubviewToBack:_message];
    
    self.message.translatesAutoresizingMaskIntoConstraints = NO;
    self.closeButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[message]-|" options:0 metrics:nil views:@{@"message" : _message}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[button(30)]-(0)-|" options:0 metrics:nil views:@{@"button" : _closeButton}]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[message]-|" options:0 metrics:nil views:@{@"message" : _message}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[button(30)]" options:0 metrics:nil views:@{@"button" : _closeButton}]];
    
}

-(void)buttonSetup{
    [self.closeButton setTitle:@"✘" forState:UIControlStateNormal];
    [self.closeButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [self.closeButton addTarget:self action:@selector(toggleButton) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - selectors
-(void)toggleButton{
    if(self.anotationDelegate){
        [self.anotationDelegate didDeleteMessage];
    }
}

@end
