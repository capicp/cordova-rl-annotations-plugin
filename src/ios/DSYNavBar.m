//
//  DSYNavBar.m
//  MyApp
//
//  Created by Alfredo Luco on 02-11-17.
//

#import "DSYNavBar.h"

@interface DSYNavBar ()

@property (readwrite,nonatomic) UIButton* deleteButton;
@property (readwrite,nonatomic) UIButton* sendButton;

@end

@implementation DSYNavBar

-(instancetype)init{
    if(!(self = [super init]))
        return nil;
    if(self){
        [self setup];
    }
    return self;
}

#pragma mark - setup
-(void)setup{
    UIView* view = [[UIView alloc] init];
    [view setBackgroundColor:[UIColor colorWithRed:0.459 green:0.455 blue:0.478 alpha:1.0]];
    
    self.deleteButton = [[UIButton alloc] init];
    [self.deleteButton setImage:[UIImage imageNamed:@"remove-symbol.png"] forState:UIControlStateNormal];
    [self.deleteButton addTarget:self action:@selector(delete) forControlEvents:UIControlEventTouchUpInside];
    self.deleteButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.deleteButton.contentVerticalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    self.sendButton = [[UIButton alloc] init];
    [self.sendButton setTitle:@"Enviar" forState:UIControlStateNormal];
    [self.sendButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [self.sendButton addTarget:self action:@selector(send) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:self.deleteButton];
    [view addSubview:self.sendButton];
    
    self.deleteButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.sendButton.translatesAutoresizingMaskIntoConstraints = NO;
    view.translatesAutoresizingMaskIntoConstraints = NO;
    
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[delete]" options:0 metrics:nil views:@{@"delete" : self.deleteButton}]];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[send]-|" options:0 metrics:nil views:@{@"send" : self.sendButton}]];
    
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[delete]-|" options:0 metrics:nil views:@{@"delete" : self.deleteButton}]];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[send]-|" options:0 metrics:nil views:@{@"send" : self.sendButton}]];
    
    [self addSubview:view];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[view]-(0)-|" options:0 metrics:nil views:@{@"view" : view}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view" : view}]];
}

#pragma mark - selectors
-(void)delete{
    if(self.delegate){
        [self.delegate didDeleteAnnotation];
    }
}

-(void)send{
    if(self.delegate){
        [self.delegate didSendAnnotation];
    }
}

@end
