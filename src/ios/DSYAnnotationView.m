//
//  DSYAnnotationView.m
//  MyApp
//
//  Created by Alfredo Luco on 31-10-17.
//

#import "DSYAnnotationView.h"
#import "DSYTextAnnotationView.h"
#import "DSYAudioManager.h"
#import "DSYAudioAnnotationView.h"
#import <AudioToolbox/AudioServices.h>

@interface DSYAnnotationView ()<UITextViewDelegate,AudioManagerDelegate,DSYAudioAnnotationViewDelegate>

@property (readwrite,nonatomic) DSYTextAnnotationView* textAnnotation;
@property (readwrite, nonatomic) DSYAudioAnnotationView* audioAnnotation;
@property (readwrite, nonatomic) NSLayoutConstraint* heightConstraint;

@end

@implementation DSYAnnotationView

-(instancetype)init{
    if(!(self = [super init])){
        return nil;
    }
    if(self){
        _audiomgr =  [DSYAudioManager sharedInstace];
        //init the audio manager
        _audiomgr.delegate = self;
        //config it
        [_audiomgr recorderConfigurationAtFileName:@"MyAudioMemo.m4a"];
        _textAnnotation = nil;
        _audioAnnotation = nil;
        _isRecording = YES;
        [self setup];
    }
    return self;
}

#pragma mark - annotation view methods
-(BOOL)hasTextAnnotation{
    return self.textAnnotation != nil;
}

-(BOOL)hasAudioAnnotation{
    return self.audioAnnotation != nil;
}

-(NSString *)getTextAnnotation{
    return self.textAnnotation.message.text != nil ? self.textAnnotation.message.text : @"";
}

-(AVURLAsset *)getAudioAnnotation{
    DSYAudioManager* audiommgr = [DSYAudioManager sharedInstace];
    return [audiommgr getAudiosWithExtension:@"m4a"].firstObject;
}

#pragma mark - setup
-(void)setup{
    
    self.bottomTextView = [[UITextView alloc] init];
    self.bottomButton = [[UIButton alloc] init];
    
    [self setupButton:_bottomButton];
    [self setupTextView:self.bottomTextView];
    
    [self addSubview: self.bottomTextView];
    [self addSubview: _bottomButton];
    
    self.bottomTextView.translatesAutoresizingMaskIntoConstraints = NO;
    self.bottomButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[textfield]-(8)-[button(50)]-|" options:0 metrics:nil views:@{@"textfield":_bottomTextView,@"button":_bottomButton}]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[textfield]-|" options:0 metrics:nil views:@{@"textfield":_bottomTextView}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[button(25)]-|" options:0 metrics:nil views:@{@"button":_bottomButton}]];
    
    _heightConstraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:44.0];
    _heightConstraint.identifier = @"bottomHeight";
    [self addConstraint:_heightConstraint];
}

-(void)setupButton:(UIButton*)button{
    [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    button.imageView.clipsToBounds = YES;
    button.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    UILongPressGestureRecognizer* longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(addAudio:)];
//    longPress.minimumPressDuration = 2.0;
    
    if(self.isRecording){
        [button setTitle:@"" forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"microphone.png"] forState:UIControlStateNormal];
        [button removeTarget:self action:@selector(sendText) forControlEvents:UIControlEventTouchUpInside];
        [button addGestureRecognizer:longPress];
    }else{
        [button setImage:nil forState:UIControlStateNormal];
        [button setTitle:@"Listo" forState:UIControlStateNormal];
        [button removeGestureRecognizer:longPress];
        [button addTarget:self action:@selector(sendText) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)setupTextView: (UITextView*)textView{
    textView.layer.cornerRadius = 4.0;
    [textView setBackgroundColor:[UIColor whiteColor]];
    textView.delegate = self;
}

#pragma mark - TextField Delegate

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    NSString *resultStr = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if(![resultStr  isEqual: @""]){
        _isRecording = NO;
        [self setupButton:_bottomButton];
    }else{
        _isRecording = YES;
        [self setupButton:_bottomButton];
    }
    _heightConstraint.constant = 100.0;
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    if([textView.text  isEqual: @""]){
        _heightConstraint.constant = 44.0;
    }
}

#pragma mark - Selectors
-(void)sendText{
    NSLog(@"Enviar");
    
    for (NSLayoutConstraint* constraint in self.constraints) {
        if([constraint.identifier  isEqual: @"bottomHeight"]){
            constraint.constant = 100.0;
            break;
        }
    }
    
    self.textAnnotation = [[DSYTextAnnotationView alloc] initWithMessage:_bottomTextView.text];
    self.textAnnotation.translatesAutoresizingMaskIntoConstraints = NO;
    self.textAnnotation.frame = self.bounds;
    self.textAnnotation.anotationDelegate = self;
    [self addSubview:self.textAnnotation];
    [self bringSubviewToFront:self.textAnnotation];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[annotation]-|" options:0 metrics:nil views:@{@"annotation" : self.textAnnotation}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[annotation]-|" options:0 metrics:nil views:@{@"annotation" : self.textAnnotation}]];
    [self.bottomTextView resignFirstResponder];
}

-(void)addAudio:(UILongPressGestureRecognizer*)gesture{
    
    if(gesture.state == UIGestureRecognizerStateBegan){
        NSURL* audioURL = [[self.audiomgr getAudiosWithExtension:@"wav"].firstObject URL];
        [self.audiomgr playAudio:audioURL withAudioTracker:YES];
    }
    
    self.bottomTextView.backgroundColor = [UIColor colorWithRed:0.459 green:0.455 blue:0.478 alpha:1.0];
    self.bottomTextView.text = @"Grabando....";
    [self.bottomTextView setUserInteractionEnabled:NO];
    
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
//    [self.audiomgr recordAudio];
    NSLog(@"recording");
    if(gesture.state == UIGestureRecognizerStateEnded){
        self.bottomTextView.backgroundColor = [UIColor whiteColor];
        self.bottomTextView.text = @"";
        [self.bottomTextView setUserInteractionEnabled:YES];
        NSURL* audioURL = [[self.audiomgr getAudiosWithExtension:@"wav"].firstObject URL];
        [self.audiomgr playAudio:audioURL withAudioTracker:NO];
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
//        [self.audiomgr stopRecording];
    }
}

#pragma mark - text annotation delegate
-(void)didDeleteMessage{
    for (NSLayoutConstraint* constraint in self.constraints) {
        if([constraint.identifier  isEqual: @"bottomHeight"]){
            constraint.constant = 44.0;
            break;
        }
    }
    [self.textAnnotation removeFromSuperview];
    self.textAnnotation = nil;
    _isRecording = YES;
    self.bottomTextView.text = @"";
    [self setupButton:_bottomButton];
}

#pragma mark - audio manager delegate
-(void)didFinished{
    [self.textAnnotation removeFromSuperview];
    self.textAnnotation = nil;
    
    //aqui agregar el reproductor
    self.audioAnnotation = [[DSYAudioAnnotationView alloc] init];
    self.audioAnnotation.delegate = self;
    self.audioAnnotation.frame = self.bounds;
    [self addSubview:self.audioAnnotation];
    [self bringSubviewToFront:self.audioAnnotation];
}

#pragma mark - audio annotation delegate
-(void)didPlayAudio{
    self.audiomgr = [DSYAudioManager sharedInstace];
    [self.audiomgr playAudio:[[self.audiomgr getAudiosWithExtension:@"m4a"].firstObject URL] withAudioTracker:NO];
}

-(void)didStopAudio{
    self.audiomgr = [DSYAudioManager sharedInstace];
    [[self.audiomgr player] pause];
}

-(void)didDeleteAudio{
    self.audiomgr = [DSYAudioManager sharedInstace];
    [self.audiomgr removeFiles];
    [self.audioAnnotation removeFromSuperview];
    self.audioAnnotation = nil;
}
@end
