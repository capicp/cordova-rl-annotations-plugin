//
//  DSYWebView.h
//  MyApp
//
//  Created by Alfredo Luco on 31-10-17.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface DSYWebView : UIView<WKUIDelegate,WKNavigationDelegate>

-(instancetype)initWithUrl:(NSURL*)url;
-(void)setup:(NSURL*)url;

@end
