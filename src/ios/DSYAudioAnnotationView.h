//
//  DSYAudioAnnotationView.h
//  MyApp
//
//  Created by Alfredo Luco on 02-11-17.
//

#import <UIKit/UIKit.h>

@protocol DSYAudioAnnotationViewDelegate<NSObject>

-(void)didDeleteAudio;
-(void)didPlayAudio;
-(void)didStopAudio;

@end

@interface DSYAudioAnnotationView : UIView

@property  (weak, nonatomic) id<DSYAudioAnnotationViewDelegate> delegate;

@end
