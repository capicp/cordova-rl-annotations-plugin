//
//  DSYAnnotationManagerPlugin.h
//  MyApp
//
//  Created by Alfredo Luco on 02-11-17.
//

#import <Cordova/CDVPlugin.h>
#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>

@interface DSYAnnotationManagerPlugin : CDVPlugin<WKNavigationDelegate,WKUIDelegate>

@property (readwrite) BOOL isRecording;
@property (readwrite) UIButton* recorderButton;
@property (readwrite) UITextField* textField;

- (void)openResource: (CDVInvokedUrlCommand *)command;
- (void)annotationSentOK: (CDVInvokedUrlCommand*)command;
- (void)annotationSentFail: (CDVInvokedUrlCommand*)command;


@end
