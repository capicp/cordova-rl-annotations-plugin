//
//  DSYAnnotationManagerPlugin.m
//  MyApp
//
//  Created by Alfredo Luco on 02-11-17.
//

#import "DSYAnnotationManagerPlugin.h"
#import <Cordova/CDVAvailability.h>
#import <UIKit/UIKit.h>
#import <SafariServices/SafariServices.h>
#import <WebKit/WebKit.h>
#import "DSYWebView.h"
#import "DSYAnnotationView.h"
#import "DSYNavBar.h"
#import "DSYAudioManager.h"
#import "DSYTextAnnotationView.h"
#import "DSYAudioAnnotationView.h"

@interface DSYAnnotationManagerPlugin ()<DSYNavBarDelegate>
@property (readwrite,nonatomic) NSString* callback;
@property (readwrite,nonatomic) NSURL* url;
@property (readwrite,nonatomic) UIView* bottomView;
@property (readwrite,nonatomic) NSArray* bottomConstraints;
@property (readwrite, nonatomic) DSYWebView* webContainer;
@property (readwrite, nonatomic) DSYAnnotationView* bottomBar;
@property (readwrite,nonatomic) DSYNavBar* navbar;
@property (readwrite,nonatomic) UIAlertController* annotationStatus;
@end

@implementation DSYAnnotationManagerPlugin

#pragma mark - js methods
- (void)openResource: (CDVInvokedUrlCommand *)command{
    
    _isRecording = YES;
    
    NSString* urlString = [command.arguments objectAtIndex: 0];
    _callback = command.callbackId;
    
    UIViewController* vc = self.getTopPresentedViewController;
    
    if(urlString == nil){
        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"url can't be empty"] callbackId:command.callbackId];
        return;
    }
    
    if (![[urlString lowercaseString] hasPrefix:@"http"]) {
        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"url must start with http or https"] callbackId:command.callbackId];
        return;
    }
    
    _url = [NSURL URLWithString:urlString];
    if (_url == nil) {
        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"bad url"] callbackId:command.callbackId];
        return;
    }
    
    
    [self addSubviews:vc webURL:_url];
    
}

- (void)annotationSentOK: (CDVInvokedUrlCommand*)command{
    [self.annotationStatus dismissViewControllerAnimated:YES completion:nil];
    self.annotationStatus = [UIAlertController alertControllerWithTitle:@"Anotación enviada" message:nil preferredStyle:UIAlertControllerStyleAlert];
    [self.annotationStatus addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
    [[self getTopPresentedViewController] presentViewController:self.annotationStatus animated:YES completion:nil];
    
    self.bottomBar.isRecording = YES;
    self.bottomBar.bottomTextView.text = @"";
    [self.bottomBar setupButton:self.bottomBar.bottomButton];
    
    for (id view in self.bottomBar.subviews) {
        NSLog(@"%@",[view class]);
        if([view isKindOfClass:[DSYTextAnnotationView class]] || [view isKindOfClass:[DSYAudioAnnotationView class]]){
            [view removeFromSuperview];
            break;
        }
    }
    for (NSLayoutConstraint *constraint in self.bottomBar.constraints) {
        if([constraint.identifier  isEqual: @"bottomHeight"]){
            constraint.constant = 44.0;
            break;
        }
    }
}

- (void)annotationSentFail: (CDVInvokedUrlCommand*)command{
    [self.annotationStatus dismissViewControllerAnimated:YES completion:nil];
    self.annotationStatus = [UIAlertController alertControllerWithTitle:@"No se ha podido enviar la anotación" message:nil preferredStyle:UIAlertControllerStyleAlert];
    [self.annotationStatus addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
    [[self getTopPresentedViewController] presentViewController:self.annotationStatus animated:YES completion:nil];
}

#pragma mark - setup
-(void)addSubviews: (UIViewController*)mainVC webURL:(NSURL*)url{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasHidden:) name:UIKeyboardDidHideNotification object:nil];
    
    self.webContainer = [[DSYWebView alloc] initWithUrl:url];
    self.bottomBar = [[DSYAnnotationView alloc] init];
    self.navbar = [[DSYNavBar alloc] init];
    self.navbar.delegate = self;
    
    [mainVC.view addSubview: self.webContainer];
    [mainVC.view addSubview: self.bottomBar];
    [mainVC.view addSubview:self.navbar];
    
    [mainVC.view bringSubviewToFront:self.webContainer];
    [mainVC.view bringSubviewToFront:self.bottomBar];
    [mainVC.view bringSubviewToFront:self.navbar];
    
    self.webContainer.translatesAutoresizingMaskIntoConstraints = NO;
    self.bottomBar.translatesAutoresizingMaskIntoConstraints = NO;
    self.navbar.translatesAutoresizingMaskIntoConstraints = NO;
    
    [mainVC.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[webView]-(0)-|" options:0 metrics:nil views:@{@"webView":_webContainer}]];
    [mainVC.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[chatView]-(0)-|" options:0 metrics:nil views:@{@"chatView":self.bottomBar}]];
    [mainVC.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[nav]-(0)-|" options:0 metrics:nil views:@{@"nav" : self.navbar}]];
    
    self.bottomConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[nav(44)]-(0)-[webView]-(0)-[chatView]-|" options:0 metrics:nil views:@{@"nav": self.navbar,@"webView":_webContainer,@"chatView":self.bottomBar}];
    
    [mainVC.view addConstraints:self.bottomConstraints];
    
    self.bottomBar.backgroundColor = [UIColor colorWithRed:0.459 green:0.455 blue:0.478 alpha:1.0];
    self.webContainer.backgroundColor = [UIColor redColor];
    
}

#pragma mark - keyboardNotifications
- (void)keyboardWasShown:(NSNotification *)notification{
    // Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    //Given size may not account for screen rotation
    [[self getTopPresentedViewController].view removeConstraints:self.bottomConstraints];
    self.bottomConstraints = [NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-[nav(44)]-(0)-[webView]-(0)-[chatView]-(%f)-|",keyboardSize.height] options:0 metrics:nil views:@{@"nav" : self.navbar,@"webView":_webContainer,@"chatView":self.bottomBar}];
    [[self getTopPresentedViewController].view addConstraints:self.bottomConstraints];
}

-(void)keyboardWasHidden:(NSNotification *)notification{
    [[self getTopPresentedViewController].view removeConstraints:self.bottomConstraints];
    self.bottomConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[nav(44)]-(0)-[webView]-(0)-[chatView]-|" options:0 metrics:nil views:@{@"nav" : self.navbar,@"webView":_webContainer,@"chatView":self.bottomBar}];
    [[self getTopPresentedViewController].view addConstraints:self.bottomConstraints];
}

#pragma mark - nav bar delegate
-(void)didSendAnnotation{
    
    NSDictionary* dictionary;
    
    //    NSString* message = [NSString stringWithFormat:@""];
    
    if(self.bottomBar.hasTextAnnotation || self.bottomBar.hasAudioAnnotation){
        if(![self.bottomBar.getTextAnnotation  isEqual: @""]){
            dictionary = @{@"message" : self.bottomBar.getTextAnnotation,@"type" : @"text"};
            //            message = self.bottomBar.getTextAnnotation;
        }else if (![self.bottomBar.getAudioAnnotation.URL.absoluteString  isEqual: @""]){
            dictionary = @{@"message" : self.bottomBar.getAudioAnnotation.URL.absoluteString,@"type" : @"audio"};
            //            message = self.bottomBar.getAudioAnnotation.URL.absoluteString;
        }
        
        //        CDVPluginResult * pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:message];
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dictionary];
        [pluginResult setKeepCallbackAsBool:YES];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:_callback];
        self.annotationStatus = [UIAlertController alertControllerWithTitle:@"Cargando anotación" message:nil preferredStyle:UIAlertControllerStyleAlert];
        [[self getTopPresentedViewController] presentViewController:self.annotationStatus animated:YES completion:nil];
    }
    
}

-(void)didDeleteAnnotation{
    [self deleteAnnotations];
}

#pragma mark - methods
-(UIViewController *)getTopPresentedViewController {
    UIViewController *presentingViewController = self.viewController;
    while(presentingViewController.presentedViewController != nil && ![presentingViewController.presentedViewController isBeingDismissed])
    {
        presentingViewController = presentingViewController.presentedViewController;
    }
    return self.viewController;
}

-(void)deleteAnnotations{
    [[self getTopPresentedViewController].view endEditing:YES];
    [self.webContainer removeFromSuperview];
    [self.bottomBar removeFromSuperview];
    [self.navbar removeFromSuperview];

    self.webContainer = nil;
    self.bottomBar = nil;
    self.navbar = nil;
    
    DSYAudioManager* audiomgr = [DSYAudioManager sharedInstace];
    [audiomgr removeFiles];
}

@end
