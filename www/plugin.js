
var exec = require('cordova/exec');

var PLUGIN_NAME = 'DSYAnnotationManagerPlugin';
var PLUGIN_TEST_NAME = 'DSYAnnotationManagerPluginTest';

var DSYAnnotationManager = {
   openResource: function(url,cb){
       exec(cb,null,PLUGIN_NAME,'openResource',[url]);
   },
   annotationSentOK: function(){
     exec(null,null,PLUGIN_NAME,'annotationSentOK',[]);
   },
   annotationSentFail: function(){
     exec(null,null,PLUGIN_NAME,'annotationSentFail',[]);
   }
};

// var DSYAnnotationManagerTest = {
//   getAnnotation: function(url,cb){
//     exec(cb,null,PLUGIN_TEST_NAME,'getAnnotation',[url]);
//   },
//   annotationSentOK: function(){
//     exec(null,null,PLUGIN_TEST_NAME,'annotationSentOK',[]);
//   },
//   annotationSentFail: function(){
//     exec(null,null,PLUGIN_TEST_NAME,'annotationSentFail',[]);
//   }
// };

/*
var MyCordovaPlugin = {
  echo: function(phrase, cb) {
    exec(cb, null, PLUGIN_NAME, 'echo', [phrase]);
  },
  getDate: function(cb) {
    exec(cb, null, PLUGIN_NAME, 'getDate', []);
  }
};
*/
module.exports = DSYAnnotationManager;
// module.exports = {
//   DSYAnnotationManager,
//   DSYAnnotationManagerTest
// };
