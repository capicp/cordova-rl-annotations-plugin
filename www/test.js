var exec = require('cordova/exec');

var PLUGIN_TEST_NAME = 'DSYAnnotationManagerPluginTest';

var DSYAnnotationManagerTest = {
  getAnnotation: function(cb){
    exec(cb,null,PLUGIN_TEST_NAME,'getAnnotation',[]);
  }
};

module.exports = DSYAnnotationManagerTest;
